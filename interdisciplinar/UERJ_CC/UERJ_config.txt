### field default for read of property 
[scriptsucupira]

diretorio_de_armazenamento_de_cvs : ./cache/interdisciplinar/UERJ_CC/cache 
diretorio_de_armazenamento_de_doi : ./cache/interdisciplinar/UERJ_CC/doi 

email_responsavel_conjunto_cursos : UERJ_CC@UERJ_CC.br

modelo_aluno : ./modelos/AL-Alunos.config
modelo_professor: ./modelos/CV_Prof.config
modelo_linha_pesquisa: ./modelos/LP-XXXX.config
modelo_programa_com_alunos: ./modelos/PR-AL-Programa.config
modelo_programas_com_alunos: ./modelos/PR-AL-Programas.config
modelo_programa: ./modelos/PR-Programa.config
modelo_programas: ./modelos/PR-Programas.config
modelo_programa_datas: ./modelos/PR-Programa-Datas.config
modelo_programas_datas: ./modelos/PR-Programas-Datas.config

dataset_professores: ./interdisciplinar/UERJ-CC/UERJ_CC_docentes.csv
dataset_alunos: 
separador_csv: |

Universidade_Abreviado : Quadrienio
Universidade_Completo : UNIVERSIDADE DO ESTADO DO RIO DE JANEIRO

############ Exatas #######################
Programa_Abreviado_1 : MD-UERJ_CC
Programa_Completo_1 : UERJ_CC
Programa_Alunos_1:

Programa_Abreviado_11 : CC
Programa_Completo_11 : CIÊNCIAS COMPUTACIONAIS
Programa_Alunos_11: M-CC, D-CC
Programa_arquivo_qualis_de_periodicos_csv_11 : ./qualis/qualis_interdisciplinar_journal_2014.csv
Programa_arquivo_qualis_de_periodicos_pdf_11 : ./qualis/qualis_interdisciplinar_journal_2014.pdf
Programa_area_de_avaliacao_qualis_11: Interdisciplinar