### field default for read of property 
[scriptsucupira]

diretorio_de_armazenamento_de_cvs : ./cache/interdisciplinar/UECE/cache 
diretorio_de_armazenamento_de_doi : ./cache/interdisciplinar/UECE/doi 

email_responsavel_conjunto_cursos : UECE@UECE.br

modelo_aluno : ./modelos/AL-Alunos.config
modelo_professor: ./modelos/CV_Prof.config
modelo_linha_pesquisa: ./modelos/LP-XXXX.config
modelo_programa_com_alunos: ./modelos/PR-AL-Programa.config
modelo_programas_com_alunos: ./modelos/PR-AL-Programas.config
modelo_programa: ./modelos/PR-Programa.config
modelo_programas: ./modelos/PR-Programas.config
modelo_programa_datas: ./modelos/PR-Programa-Datas.config
modelo_programas_datas: ./modelos/PR-Programas-Datas.config

dataset_professores: ./interdisciplinar/UECE/UECE_docentes.csv
dataset_alunos: 
separador_csv: |

Universidade_Abreviado : Quadrienio
Universidade_Completo : UNIVERSIDADE ESTADUAL DO CEARÁ

############ Exatas #######################
Programa_Abreviado_1 : MD-UECE
Programa_Completo_1 : UECE
Programa_Alunos_1:

Programa_Abreviado_11 : CFA
Programa_Completo_11 : CIÊNCIAS FÍSICAS APLICADAS
Programa_Alunos_11: M-CFA, D-CFA
Programa_arquivo_qualis_de_periodicos_csv_11 : ./qualis/qualis_interdisciplinar_journal_2014.csv
Programa_arquivo_qualis_de_periodicos_pdf_11 : ./qualis/qualis_interdisciplinar_journal_2014.pdf
Programa_area_de_avaliacao_qualis_11: Interdisciplinar