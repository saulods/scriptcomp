'''
Created on 15 de jun de 2017
'''
import sys
import os
import csv
import parse as processObject
import conversions as convert
from scipy.cluster.hierarchy import linkage
import hierarchicalClusteringUtil as hcutil
from distances import scriptDistOrientacao 
from distances import scriptDistArtigosQualis
from distances import scriptDistArtigosQualisAtualizado
from distances import scriptDistCursos


def lerCSV(file,separador):
    d={}
    with open(file,'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter=separador, quoting=csv.QUOTE_NONE)
        for row in rows:
            k, v = row
            d[k] = v
    return d

def findNotaCapes(curso,csvDict):
    return csvDict[curso[3:]]+':'+curso[3:], curso[3:]+':'+csvDict[curso[3:]]



def processarUnicoAgrupamento(caminhoDosCurriculos):
    listaDeCurriculos = []
    for d in os.listdir(caminhoDosCurriculos):
        arquivoDataSource = None
        arquivoPublicacoesPorMembro = None
         
        for f in os.listdir(caminhoDosCurriculos+d):
            if f.endswith('database.xml'):
                arquivoDataSource = caminhoDosCurriculos+d+'/'+f                
            if f.endswith('publicacoesPorMembro.csv'): 
                arquivoPublicacoesPorMembro = caminhoDosCurriculos+d+'/'+f
        if arquivoDataSource!=None and arquivoPublicacoesPorMembro !=None:
            xml = processObject.preparXML(open(arquivoDataSource))
            curriculo = processObject.parseXMLScriptLattes(xml)
            processObject.parseCSVScriptLattes(curriculo, arquivoPublicacoesPorMembro, "\t")
            listaDeCurriculos = listaDeCurriculos+[curriculo]
                       
    listaQtdOriMestrConcluido = convert.convertListaQuantidadeOrientacaoMestradoConcluido(listaDeCurriculos)
    
    listaQualis = convert.convertListaQuantidadeArtigosQualis(listaDeCurriculos)
    #quantidadeDeDocentesPermanetes
    quantidadeDeCurriculos =len(listaQualis) 

    treeQtdOriMestrConcluido = linkage(listaQtdOriMestrConcluido, method='single', metric= lambda u, v: scriptDistOrientacao(u,v))
    
    treeQualis = linkage(listaQualis, method='single', metric= lambda u, v: scriptDistArtigosQualisAtualizado(u,v,quantidadeDeCurriculos))
    
    hcutil.plotDendrogram(treeQtdOriMestrConcluido, 0.5,  "by beta");
    hcutil.plotDendrogram(treeQualis, 0,  "by Qualis");
    
def processarListaDeCurriculos(caminhoDosCurriculos):
    listaDeCurriculos = []
    
    try:
        for d in os.listdir(caminhoDosCurriculos):
            if(not d.startswith('.')):
                arquivoDataSource = None
                arquivoPublicacoesPorMembro = None
                 
                for f in os.listdir(caminhoDosCurriculos+'/'+d):
                    if f.endswith('database.xml'):
                        arquivoDataSource = caminhoDosCurriculos+'/'+d+'/'+f                
                    if f.endswith('publicacoesPorMembro.csv'): 
                        arquivoPublicacoesPorMembro = caminhoDosCurriculos+'/'+d+'/'+f
                if arquivoDataSource!=None and arquivoPublicacoesPorMembro !=None:
                    xml = processObject.preparXML(open(arquivoDataSource))
                    curriculos = processObject.parseXMLScriptLattesAll(xml)
                    for curriculo in curriculos:
                        processObject.parseCSVScriptLattes(curriculo, arquivoPublicacoesPorMembro, "\t")
                        listaDeCurriculos = listaDeCurriculos+[curriculo]
    except:
        print "Erro no diretorio "+caminhoDosCurriculos
    
        
    return listaDeCurriculos 


def processarLista(listaDeCurso,labels, descricao, listOriginal):
    tree = linkage(listaDeCurso, method='average', metric= lambda u, v: scriptDistCursos(u,v))
    hcutil.plotDendrogramTest(tree, 0,  descricao, labels, listOriginal);
    

def processarAgrupamentoPorCurso(listaDeCurso, descricao, labels, color_threshold):
    tree = linkage(listaDeCurso, method='average', metric= lambda u, v: scriptDistCursos(u,v))
    hcutil.plotDendrogramTest(tree, 0,  descricao, labels, listaDeCurso,color_threshold );
    
def printAnalise(count,descricao,qtdProfessores, lista):
    for nota in lista:
        print str(count)+'|'+descricao+'|' +str(len(qtdProfessores)+'|'+nota )
    
    
if __name__ == '__main__':
        
    if(len(sys.argv)>2):
        listasDeCurriculos = []
        
        dictCursos={}
        count = 1
        listasDePublicacoesPorCurso=[]
        listasDePublicacoesPorCursoSUP=[]
        listasDePublicacoesPorCursoOri=[]
        listasDePublicacoesPorCursoInd=[]
        
        dirAno = sys.argv[2]
        
        dictNotasCapes =lerCSV('./notaCapes.csv','|')
        
        profPermaDict = lerCSV('./Permanente.csv', '|')
        #print profPermaDict
        
        labelsR =[]
        labels =[]
        #print 'indice|curso|qtd_prof|IndProdArt|IndProdSup|Orientacao|IndProd'
        for pasta in os.listdir(sys.argv[1]):
            if(not pasta.startswith('.')):
                #print ''+sys.argv[1]+d+'/2013-2016'
                
                qtdProfessoresPermanentes = int(profPermaDict[pasta[3:]])
                
                listaProcessada = processarListaDeCurriculos(sys.argv[1]+pasta+'/'+ dirAno)
                
                valorProcessadoArray = convert.convertListaMediaDeProducaoesQualis(listaProcessada, count, qtdProfessoresPermanentes)
                listasDePublicacoesPorCurso = listasDePublicacoesPorCurso+[valorProcessadoArray]    
                
                valorProcessadoArraySup = convert.convertListaMediaDeProducaoesQualisSup(listaProcessada, count, qtdProfessoresPermanentes)
                listasDePublicacoesPorCursoSUP = listasDePublicacoesPorCursoSUP+[valorProcessadoArraySup]    

                valorProcessadoArrayOri = convert.convertListaQuantidadeOrientacoes(listaProcessada, qtdProfessoresPermanentes)
                listasDePublicacoesPorCursoOri = listasDePublicacoesPorCursoOri+[valorProcessadoArrayOri]    
                
                valorProcessadoArrayIndProd = convert.converstIndProdutividadeDoPrograma(listaProcessada, qtdProfessoresPermanentes)
                listasDePublicacoesPorCursoInd = listasDePublicacoesPorCursoInd+[valorProcessadoArrayIndProd]  

                #labels=labels+[pasta+'('+str(valorProcessadoArray)+')']  
                descrCursoR,descrCurso =findNotaCapes(pasta, dictNotasCapes)
                
                #descrCurso = pasta[3:]+'+'+str(valorProcessadoArrayIndProd)[:6]
                labelsR=labelsR+[descrCursoR]  
                labels=labels+[descrCurso]  
                #print str(count)+'|'+descrCurso+'|'+str(len(listaProcessada))+'|'+str(valorProcessadoArray)+'|'+str(valorProcessadoArraySup)+'|'+str(valorProcessadoArrayOri)+'|'+str(valorProcessadoArrayIndProd)
                print count
                count+=1

        #print listasDePublicacoesPorCurso      
        #print labels

        processarAgrupamentoPorCurso(listasDePublicacoesPorCurso, 'Por indProdArt '+dirAno,labels, 0.3)        
        processarAgrupamentoPorCurso(listasDePublicacoesPorCursoSUP, 'Por indProdArtSup '+dirAno,labels, 0.4 )
        processarAgrupamentoPorCurso(listasDePublicacoesPorCursoOri, 'Por Orientacoes '+dirAno,labels, 0.2)
        processarAgrupamentoPorCurso(listasDePublicacoesPorCursoInd, 'Por indProd '+dirAno,labels, 0.4)

        #print listasDePublicacoesPorCursoInd
        #processarLista(listasDePublicacoesPorCursoInd, 'Por indProd',labels,valorProcessadoArrayIndProd)

        #print listasDePublicacoesPorCurso  
    else:
        caminhoDosCurriculos = sys.argv[1]
        #sys.argv[1]
        processarUnicoAgrupamento(caminhoDosCurriculos)  
    
    
    
    
    
    
    
    pass