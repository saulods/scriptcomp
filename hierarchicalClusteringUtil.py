'''
Created on 29 de ago de 2017

@author: mac
'''
import pandas
import numpy as np
from scipy.cluster.hierarchy import dendrogram, fcluster, to_tree, leaves_list
from scipy import array
from matplotlib import pyplot as plt    
from matplotlib.backends.backend_pdf import PdfPages



def writeCsvByCluster(clusterData, csvFileBaseName, csvFileNameByClusterJoin):
    dataSetBase = pandas.read_csv(csvFileBaseName,sep=";");
    df = pandas.DataFrame(clusterData,columns=['Clusters']) # A is a numpy 2d array
    df=df.join(dataSetBase)
    df.to_csv(csvFileNameByClusterJoin,';',index=False) 
    
def clusterGenerate(tree, k):
    clusters = fcluster(tree, k, criterion='maxclust')
    return clusters

def plotDendrogram(tree, p, descriptionDendrogram):
    plt.figure(figsize=(5, 5))
    plt.title('Hierarchical Clustering Dendrogram \n{}'.format(descriptionDendrogram))
    plt.xlabel('Curriculum number of clusters')
    plt.ylabel('Clusters distance')
    dendrogram(tree,p,show_contracted=False, truncate_mode='lastp', leaf_font_size=8., show_leaf_counts=True)
    plt.show()
    

    
def plotCluster(tree,size):
    plt.figure(figsize=(5, 5))
    plt.title('AHExemplo')
    plt.xlabel('Curriculum number of clusters')
    plt.ylabel('Clusters distance')
    fcluster(tree, size, criterion="distance")
    plt.show()


def plot_tree( P, pos=None ):
    icoord = array( P['icoord'] )
    dcoord = array( P['dcoord'] )
    color_list = array( P['color_list'] )
    xmin, xmax = icoord.min(), icoord.max()
    ymin, ymax = dcoord.min(), dcoord.max()
    if pos:
        icoord = icoord[pos]
        dcoord = dcoord[pos]
        color_list = color_list[pos]
    for xs, ys, color in zip(icoord, dcoord, color_list):
        plt.plot(xs, ys,  color)
    plt.xlim( xmin-10, xmax + 0.1*abs(xmax) )
    plt.ylim( ymin, ymax + 0.1*abs(ymax) )
    plt.show()
 
 
def plotDendrogramAux(tree, p, descriptionDendrogram,labels):
    plt.figure(figsize=(25, 5))
    plt.title('Hierarchical Clustering Dendrogram \n{}'.format(descriptionDendrogram))
    plt.xlabel('Curriculum number of clusters')
    plt.ylabel('Clusters distance')
    dendrogram(tree,p,show_contracted=False, truncate_mode='lastp', color_threshold=3, orientation='right',labels=labels,
               leaf_font_size=2, show_leaf_counts=True)
    plt.show()    
       
def fancy_dendrogram(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    descricao = kwargs.pop('descricao','.')
    
    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram '+descricao)
        plt.xlabel(descricao)
        plt.ylabel('Distancia')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata
       
       
def fancy_dendrogram2(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram (truncated)')
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('distance')
        for d, i, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(d[1:3])
            y = i[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata


def plotDendrogramTest(tree, p, descriptionDendrogram,labels, listaOriginal, color_threshold, figx=17,figy=15):
    fig = plt.figure(figsize=(figx, figy))
 #   ax2 = fig.add_axes()
    
    #ax2.set_xticks([])
#    ax2.set_yticks([])
 #   ax2.axis('off')
    plt.title(' Agrupamento Hierarquico - Dendrograma de\n '+descriptionDendrogram)
    plt.xlabel('Programas Stricto Sensu')
    plt.ylabel('Distancia real')
    dend = dendrogram(tree,p,show_contracted=False, truncate_mode='lastp', color_threshold=color_threshold, labels=labels,
               leaf_font_size=10., show_leaf_counts=True)
    
    
    plotDendro(dend, tree, listaOriginal, descriptionDendrogram)
    #plt.show()  

def pre_order(x, listIds):
    if(x.is_leaf()):
        listIds=listIds+[x.id]
        return listIds
    else:
        listIds=listIds+[x.id]
        listIds = pre_order (x.left, listIds)
        return pre_order (x.right, listIds)

def plotDendro(dend, Z, listOriginal, descriptionDendrogram):
    # ----------------------------------------
# get leave coordinates, which are at y == 0
    
    pp = PdfPages(descriptionDendrogram+'.pdf')

    def flatten(l):
        return [item for sublist in l for item in sublist]
    X = flatten(dend['icoord'])
    Y = flatten(dend['dcoord'])
    leave_coords = [(x,y) for x,y in zip(X,Y) if y==0]
    
    order = np.argsort([x for x,y in leave_coords])
    id_to_coord = dict(zip(dend['leaves'], [leave_coords[idx] for idx in order])) # <- main data structure
    
    children_to_parent_coords = dict()
    for i, d in zip(dend['icoord'], dend['dcoord']):
        x = (i[1] + i[2]) / 2
        y = d[1] # or d[2]
        parent_coord = (x, y)
        left_coord = (i[0], d[0])
        right_coord = (i[-1], d[-1])
        children_to_parent_coords[(left_coord, right_coord)] = parent_coord
    
    # traverse tree from leaves upwards and populate mapping ID -> (x,y)
    root_node, node_list = to_tree(Z, rd=True)
        #print list

    #print 'NODE LIST: '
    #print node_list 
    ids_left = range(len(dend['leaves']), len(node_list))
    
    while len(ids_left) > 0:
    
        for ii, node_id in enumerate(ids_left):
            node = node_list[node_id]
            if (node.left.id in id_to_coord) and (node.right.id in id_to_coord):
                left_coord = id_to_coord[node.left.id]
                right_coord = id_to_coord[node.right.id]
                id_to_coord[node_id] = children_to_parent_coords[(left_coord, right_coord)]
    
        ids_left = [node_id for node_id in range(len(node_list)) if not node_id in id_to_coord]
    
    # plot result on top of dendrogram
    
   # print '$$$$$$$$$%%%%%%% LIST'

    ax = plt.gca()
    #plt.figure(figsize=(50, 50))

    for node_id, (x, y) in id_to_coord.iteritems():
        listIds=[]
        node = node_list[node_id]
        ids = pre_order(node,listIds)
        
        sum =0
        cont =0
        for idNode in ids:
            if node_list[idNode].is_leaf():
                sum = sum + listOriginal[idNode]
                cont = cont + 1
            
        resultId = sum / cont
        
        
        
        if not node_list[node_id].is_leaf():
            ax.plot(x, y, 'ro')
            ax.annotate("%.2g" % resultId, (x, y), xytext=(0, -8),
                        textcoords='offset points',
                        va='top', ha='center')
    
    dend['node_id_to_coord'] = id_to_coord
    ###plt.show()
    pp.savefig(dpi=199)
    pp.close()