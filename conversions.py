'''
@author: mac
'''
import numpy as np

def convertListaQuantidadeOrientacaoMestradoConcluido(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        #print count, " -->", curriculo.nome, " ", curriculo.quantidadeOrientacaoMestradoConcluido
        parameter = [curriculo.quantidadeOrientacaoMestradoConcluido]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1    
    return np.array(listArray)

def convertListaOrientacaoDoutoradoConcluido(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        #print count, " -->", curriculo.nome, " ", curriculo.quantidadeOrientacaoDoutoradoConcluido
        parameter = [curriculo.quantidadeOrientacaoDoutoradoConcluido]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)
    
def convertListaQuantidadeArtigosEmRevista(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeArtigosEmRevista]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeApresentacaoTrabalho(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeApresentacaoTrabalho]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeOrientacaoIniciacaoCientificaEmAndamento(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeOrientacaoIniciacaoCientificaEmAndamento]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeOrientacaoMestradoEmAndamento(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeOrientacaoMestradoEmAndamento]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeOrientacaoDoutoradoEmAndamento(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeOrientacaoDoutoradoEmAndamento]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeTrabalhoCompletoCongresso(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeTrabalhoCompletoCongresso]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeArtigosEmPeriodicos(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeArtigosEmPeriodicos]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeProjetosPesquisa(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeProjetosPesquisa]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeParticipacaoEvento(listaDeCurriculos):
    listArray=[]
    idHC=0
    for curriculo in listaDeCurriculos:
        parameter = [curriculo.quantidadeParticipacaoEvento]
        listArray = listArray+[parameter]
        curriculo.idHC = idHC
        idHC=idHC+1 
    return np.array(listArray)

def convertListaQuantidadeLivrosPorDocente(listaDeCurriculos, qtdProfessoresPermanentes):
    indProdCap=0

    for curriculo in listaDeCurriculos:
        indProdCap += curriculo.quantidadeLivrosPublicados

    return indProdCap/qtdProfessoresPermanentes

def convertListaQuantidadeCapitulosPorDocente(listaDeCurriculos, qtdProfessoresPermanentes):
    indProdCap=0
    for curriculo in listaDeCurriculos:
        indProdCap += curriculo.quantidadeCapitulosDeLivros

    return indProdCap/qtdProfessoresPermanentes

def convertListaQuantidadeProducoesTecnicas(listaDeCurriculos, qtdProfessoresPermanentes):
    indProdTec=0
    for curriculo in listaDeCurriculos:
        indProdTec += curriculo.quantidadeProducaoTecnica
    return indProdTec/qtdProfessoresPermanentes

def convertListaQuantidadeArtigosQualis(listaDeCurriculos):
    listArray=[]
    
    idHC=0
    for curriculo in listaDeCurriculos:
        quantidadeA1 = curriculo.quantidadeA1()
        quantidadeA2 = curriculo.quantidadeA2()
        quantidadeB1 = curriculo.quantidadeB1()
        quantidadeB2 = curriculo.quantidadeB2()
        quantidadeB3 = curriculo.quantidadeB3()
        quantidadeB4 = curriculo.quantidadeB4()
        quantidadeB5 = curriculo.quantidadeB5()
        
        parameter = [quantidadeA1]
        parameter =parameter+[quantidadeA2]
        parameter =parameter+[quantidadeB1]
        parameter =parameter+[quantidadeB2]
        parameter =parameter+[quantidadeB3]
        parameter =parameter+[quantidadeB4]
        parameter =parameter+[quantidadeB5]
        curriculo.idHC = idHC
        print curriculo.idHC, " -->", curriculo.nome, " A1: ", quantidadeA1, " A2: ", quantidadeA2, " B1: ", quantidadeB1, " B2: ", quantidadeB2, " B3: ", quantidadeB3, " B4: ", quantidadeB4, " B5: ", quantidadeB5   
        idHC=idHC+1
        listArray = listArray+[parameter]
    return np.array(listArray)


def convertQuantidadeProducaoArt(listaDeCurriculos):
    qtdProfessores = len(listaDeCurriculos)
    somaDasProducoes=0
    for curriculo in listaDeCurriculos:
        quantidadeA1 = curriculo.quantidadeA1()
        quantidadeA2 = curriculo.quantidadeA2()
        quantidadeB1 = curriculo.quantidadeB1()
        quantidadeB2 = curriculo.quantidadeB2()
        quantidadeB3 = curriculo.quantidadeB3()
        quantidadeB4 = curriculo.quantidadeB4()
        quantidadeB5 = curriculo.quantidadeB5()
        #print curriculo.idHC, " -->", curriculo.nome, " A1: ", quantidadeA1, " A2: ", quantidadeA2, " B1: ", quantidadeB1, " B2: ", quantidadeB2, " B3: ", quantidadeB3, " B4: ", quantidadeB4, " B5: ", quantidadeB5   
        somaDasProducoes = somaDasProducoes + (1*quantidadeA1 + 0.85*quantidadeA2 + 0.7*quantidadeB1 + 0.55*quantidadeB2 + 0.4*quantidadeB3 + 0.25*quantidadeB4 + 0.1*quantidadeB5)
        
    return somaDasProducoes/qtdProfessores

def convertListaMediaDeProducaoesQualis(listaDeCurriculos, idHC, qtdProfessoresPermanentes):
    listArray=[]
    somaDasProducoes=0
    for curriculo in listaDeCurriculos:
        quantidadeA1 = curriculo.quantidadeA1()
        quantidadeA2 = curriculo.quantidadeA2()
        quantidadeB1 = curriculo.quantidadeB1()
        quantidadeB2 = curriculo.quantidadeB2()
        quantidadeB3 = curriculo.quantidadeB3()
        quantidadeB4 = curriculo.quantidadeB4()
        quantidadeB5 = curriculo.quantidadeB5()
        curriculo.idHC = idHC
        #print curriculo.idHC, " -->", curriculo.nome, " A1: ", quantidadeA1, " A2: ", quantidadeA2, " B1: ", quantidadeB1, " B2: ", quantidadeB2, " B3: ", quantidadeB3, " B4: ", quantidadeB4, " B5: ", quantidadeB5   
        idHC=idHC+1
        somaDasProducoes = somaDasProducoes + (1*quantidadeA1 + 0.85*quantidadeA2 + 0.7*quantidadeB1 + 0.55*quantidadeB2 + 0.4*quantidadeB3 + 0.25*quantidadeB4 + 0.1*quantidadeB5)
        
    mediasDasProducoes = somaDasProducoes/qtdProfessoresPermanentes
    
    #for curriculo in listaDeCurriculos:
    listArray = listArray+[mediasDasProducoes]

    return np.array(listArray)


def calcListaMediaDeProducaoesQualisSup(listaDeCurriculos, idHC, qtdProfessoresPermanentes):
    somaDasProducoes=0
    for curriculo in listaDeCurriculos:
        quantidadeA1 = curriculo.quantidadeA1()
        quantidadeA2 = curriculo.quantidadeA2()
        quantidadeB1 = curriculo.quantidadeB1()
        curriculo.idHC = idHC
        #print curriculo.idHC, " -->", curriculo.nome, " A1: ", quantidadeA1, " A2: ", quantidadeA2, " B1: ", quantidadeB1, " B2: ", quantidadeB2, " B3: ", quantidadeB3, " B4: ", quantidadeB4, " B5: ", quantidadeB5   
        idHC=idHC+1
        somaDasProducoes = somaDasProducoes + (1*quantidadeA1 + 0.85*quantidadeA2 + 0.7*quantidadeB1)
        
    return somaDasProducoes/qtdProfessoresPermanentes
    

def convertListaMediaDeProducaoesQualisSup(listaDeCurriculos, idHC, qtdProfessoresPermanentes):
    listArray=[]
    #for curriculo in listaDeCurriculos:
    listArray = listArray+[calcListaMediaDeProducaoesQualisSup(listaDeCurriculos, idHC, qtdProfessoresPermanentes)]

    return np.array(listArray)



def converstIndProdutividadeDoPrograma(listaDeCurriculos, qtdProfessoresPermanentes):
    indProdArt  =  calcListaMediaDeProducaoesQualisSup(listaDeCurriculos,0, qtdProfessoresPermanentes)
    #indProdArt  =  convertListaMediaDeProducaoesQualis(listaDeCurriculos)
    indProdLiv  =  convertListaQuantidadeCapitulosPorDocente(listaDeCurriculos,qtdProfessoresPermanentes)
    indProdCap  =  convertListaQuantidadeLivrosPorDocente(listaDeCurriculos,qtdProfessoresPermanentes)
    indProdTec  = convertListaQuantidadeProducoesTecnicas(listaDeCurriculos,qtdProfessoresPermanentes)

    indProd = indProdArt + indProdLiv + indProdCap +indProdTec
    
    listArray=[indProd]
    return np.array(listArray)
    
def convertListaQuantidadeOrientacoes(listaDeCurriculos, qtdProfessoresPermanentes):
    listArray=[]
    
    orientacaoMestradoAndamento  = 0
    orientacaoMestradoConcluido  = 0
    orientacaoDoutoradoAndamento = 0
    orientacaoDoutoradoConcluido = 0
    for curriculo in listaDeCurriculos:
        #print count, " -->", curriculo.nome, " ", curriculo.quantidadeOrientacaoMestradoConcluido
        orientacaoMestradoAndamento  += curriculo.quantidadeOrientacaoMestradoEmAndamento
        orientacaoMestradoConcluido  += curriculo.quantidadeOrientacaoMestradoConcluido
        orientacaoDoutoradoAndamento += curriculo.quantidadeOrientacaoDoutoradoEmAndamento
        orientacaoDoutoradoConcluido += curriculo.quantidadeOrientacaoDoutoradoConcluido
    
    mediaOri = (orientacaoMestradoAndamento+orientacaoMestradoConcluido+orientacaoDoutoradoAndamento+orientacaoDoutoradoConcluido)/qtdProfessoresPermanentes
    listArray = listArray+[mediaOri]
    return np.array(listArray)


def convertListaIndProd(listaDeCurriculos, qtdProfessoresPermanentes):
    listArray=[]
    
    orientacaoMestradoAndamento  = 0
    orientacaoMestradoConcluido  = 0
    orientacaoDoutoradoAndamento = 0
    orientacaoDoutoradoConcluido = 0
    for curriculo in listaDeCurriculos:
        #print count, " -->", curriculo.nome, " ", curriculo.quantidadeOrientacaoMestradoConcluido
        orientacaoMestradoAndamento  += curriculo.quantidadeOrientacaoMestradoEmAndamento
        orientacaoMestradoConcluido  += curriculo.quantidadeOrientacaoMestradoConcluido
        orientacaoDoutoradoAndamento += curriculo.quantidadeOrientacaoDoutoradoEmAndamento
        orientacaoDoutoradoConcluido += curriculo.quantidadeOrientacaoDoutoradoConcluido
    
    mediaOri = (orientacaoMestradoAndamento+orientacaoMestradoConcluido+orientacaoDoutoradoAndamento+orientacaoDoutoradoConcluido)/qtdProfessoresPermanentes
    listArray = listArray+[mediaOri]
    return np.array(listArray)
