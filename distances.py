'''
Created on 29 de ago de 2017

@author: mac
'''

def scriptDistOrientacao(Ci, Cj):
    dist = abs(Ci[0] - Cj[0])
    if dist==0:
        return 0.1
    else:
        return dist  
    
def scriptDistArtigosEmRevista(Ci, Cj):
    dist = abs(Ci[0] - Cj[0])
    if dist==0:
        return 0.1
    else:
        return dist       
    
def scriptDistArtigosQualis(Ci, Cj):
    dist = (abs(1*Ci[0] - 1*Cj[0])) + (abs(0.85*Ci[1] - 0.85*Cj[1])) + (abs(0.7*Ci[2] -0.7*Cj[2])) + (abs(0.55*Ci[3] - 0.55*Cj[3])) + (abs(0.4*Ci[4] - 0.4*Cj[4])) + (abs(0.25*Ci[5] - 0.25*Cj[5])) + (abs(0.1*Ci[6] - 0.1*Cj[6]))  
    if dist==0:
        return 0.1
    else:
        return dist     


def scriptDistMediaDeProducoesQualis(Ci, Cj):
    dist = abs(Ci[0] - Cj[0])
    if dist==0:
        return 0.1
    else:
        return dist
    

def scriptDistArtigosQualisAtualizado(Ci, Cj, quantidadeDeCurriculos):
    dist = (abs(1*Ci[0] - 1*Cj[0])) + (abs(0.85*Ci[1] - 0.85*Cj[1])) + (abs(0.7*Ci[2] -0.7*Cj[2])) + (abs(0.55*Ci[3] - 0.55*Cj[3])) + (abs(0.4*Ci[4] - 0.4*Cj[4])) + (abs(0.25*Ci[5] - 0.25*Cj[5])) + (abs(0.1*Ci[6] - 0.1*Cj[6]))/quantidadeDeCurriculos  
    if dist==0:
        return 0.1
    else:
        return dist  
    
def scriptDistCursos(Ci, Cj):
    dist = abs(Ci[0] - Cj[0])
    if dist==0:
        return 0.1+0.2
    else:
        return dist+0.2